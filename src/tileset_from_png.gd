tool
extends VBoxContainer

# TODO: allow option for import a Tiled sheet instead of a PNG, much more powerful.

func _ready():
	$TransformButton.connect("button_down", self, "_on_transform_button_pressed")

func make_success_popup():
	var popup = ConfirmationDialog.new()
	popup.dialog_text = "Export complete!"
	popup.rect_position = get_viewport_rect().size / 2
	add_child(popup)
	popup.popup()
	
func make_error(reason):
	var popup = ConfirmationDialog.new()
	popup.dialog_text = "Export failed! " + reason
	add_child(popup)
	popup.rect_position = get_viewport_rect().size / 2
	popup.popup()

func _on_transform_button_pressed():
	var file_name = $FileProperties.get_filename()
	var folder_name = $FileProperties.get_foldername()

	
	var texture = $FileProperties.get_texture()
	var tile_properties = $TileProperties.get_tile_properties()
	if filename == "":
		make_error("No filename")
		return
	if tile_properties == null:
		make_error("0 is not a valid tile or sprite dimension")
		return
	if texture == null:
		make_error("No texture selected")
		return
		
	var tileset = make_tileset(texture, tile_properties)
	var sprite_scene = make_sprite_scene(texture, tile_properties)
	
	var dir = Directory.new()
	
	if not dir.dir_exists($FileProperties/SceneFolder/Preview.get_text() + "/" + folder_name):
		dir.make_dir($FileProperties/SceneFolder/Preview.get_text() + "/" + folder_name)
		
	if not dir.dir_exists($FileProperties/TilesetFolder/Preview.get_text() + "/" + folder_name):
		dir.make_dir($FileProperties/TilesetFolder/Preview.get_text() + "/" + folder_name)
	
	save_tileset_scene(sprite_scene, $FileProperties/SceneFolder/Preview.get_text(), folder_name + "/" + file_name)
	save_tileset(tileset, $FileProperties/TilesetFolder/Preview.get_text(), folder_name + "/" + file_name)
	
	make_success_popup()
		
	
# TODO: this is where the shit starts
	
func numbered_name(number):
	var s = str(number)
	# three seems realistic...
	for i in range(3 - s.length()):
		s = '0' + s
	return s
	
func calculate_rectangle(index, texture_size, sprite_size):
	var texture_width = texture_size.x
	var texture_height = texture_size.y
	
	var num_columns = int(texture_width / sprite_size.width)
	
	var row_index = int(index / num_columns)
	var column_index = int(index % num_columns)
	return Rect2(column_index * sprite_size.width, row_index * sprite_size.height, sprite_size.width, sprite_size.height)
	
func make_tileset(texture, tile_properties):
	var tileset = TileSet.new()
	var texture_size = texture.get_size()
	var offset_vector = -Vector2(tile_properties.sprite.width - tile_properties.tile.width, tile_properties.sprite.height - tile_properties.tile.height) / 2
	for i in range(amount_of_tiles(texture_size, tile_properties.sprite)):
		var rectangle = calculate_rectangle(i, texture_size, tile_properties.sprite)
		tileset.create_tile(i)
		tileset.tile_set_name(i, numbered_name(i))
		tileset.tile_set_texture(i, texture)
		tileset.tile_set_region(i, rectangle)
		tileset.tile_set_texture_offset(i, offset_vector)
	return tileset
	
func make_sprite_scene(texture, tile_properties):
	var tileset_scene = Node2D.new()
	var texture_size = texture.get_size()
	var offset_vector = -Vector2(tile_properties.sprite.width - tile_properties.tile.width, tile_properties.sprite.height - tile_properties.tile.height) / 2
	for i in range(amount_of_tiles(texture_size, tile_properties.sprite)):
		var rectangle = calculate_rectangle(i, texture_size, tile_properties.sprite)
		var sprite = Sprite.new()
		sprite.set_texture(texture)
		sprite.set_region(true)
		
		sprite.set_region_rect(rectangle)
		sprite.set_position(rectangle.position)
		sprite.set_name(numbered_name(i))
		tileset_scene.add_child(sprite)
		sprite.set_owner(tileset_scene)
		sprite.set_offset(offset_vector)
	return tileset_scene

	
func amount_of_tiles(texture_size, sprite_size):
	var texture_width = texture_size.x
	var texture_height = texture_size.y
	
	return int(texture_width / sprite_size.width * texture_height / sprite_size.height)
	
func save_tileset(tileset, folder, file_name):
	ResourceSaver.save(folder + "/" + file_name + ".tres", tileset)
			
func save_tileset_scene(tileset_scene, folder, file_name):
	var packed_scene = PackedScene.new()
	packed_scene.pack(tileset_scene)
	ResourceSaver.save(folder + "/" + file_name + ".tscn", packed_scene)
