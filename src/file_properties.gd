tool
extends VBoxContainer

onready var select_image_button = $Image/Button
onready var image_preview = $Image/Preview
onready var image_dialog = $Image/FileDialog

onready var select_scene_button = $SceneFolder/Button
onready var scene_folder_label = $SceneFolder/Preview
onready var scene_dialog = $SceneFolder/FileDialog

onready var select_tileset_button = $TilesetFolder/Button
onready var tileset_folder_label = $TilesetFolder/Preview
onready var tileset_dialog = $TilesetFolder/FileDialog

var image_path
var scene_folder = "res://src/level/2d/tilesets"
var tileset_folder = "res://assets/tilesets"

func _ready():
	image_dialog.connect("file_selected", self, "_on_image_selected")
	select_image_button.connect("button_down", self, "_on_select_image_pressed")
	image_dialog.rect_position = get_viewport_rect().size / 2
	
	scene_dialog.connect("dir_selected", self, "_on_scene_selected")
	select_scene_button.connect("button_down", self, "_on_select_scene_pressed")
	scene_dialog.rect_position = get_viewport_rect().size / 2
	scene_folder_label.set_text(scene_folder)
	
	tileset_dialog.connect("dir_selected", self, "_on_tileset_selected")
	select_tileset_button.connect("button_down", self, "_on_select_tileset_pressed")
	tileset_dialog.rect_position = get_viewport_rect().size / 2
	tileset_folder_label.set_text(tileset_folder)

#	tool_button.set_icon(file_dialog.theme)
	pass
	
func get_texture():
	if image_path == null:
		return
	var texture = load(image_path)
	return texture
	
func _on_image_selected(image):
	image_path = image
	var texture = load(image)
	image_preview.set_texture(texture)
	
func _on_scene_selected(folder):
	scene_folder = folder
	scene_folder_label.set_text(scene_folder)
	
func _on_tileset_selected(folder):
	tileset_folder = folder
	tileset_folder_label.set_text(tileset_folder)

func _on_select_image_pressed():
	image_dialog.popup()
	
func _on_select_scene_pressed():
	scene_dialog.popup()
	
func _on_select_tileset_pressed():
	tileset_dialog.popup()
	
func get_filename():
	return $FileName/Value.get_text()
	
func get_foldername():
	return $FolderName/Value.get_text()
