tool
extends VBoxContainer

onready var template_options = $Templates

onready var sprite_width = $SpriteWidth/Value
onready var sprite_height = $SpriteHeight/Value

onready var tile_width = $TileWidth/Value
onready var tile_height = $TileHeight/Value

# very custom for BB right now...
const TEMPLATES = [{
		"label": "NONE",
		"sprite": {
			"width": 0,
			"height": 0,
		},
		"tile": {
			"width": 0,
			"height": 0,
		},
	},{
		"label": "INFORMATION",
		"sprite": {
			"width": 64,
			"height": 32,
		},
		"tile": {
			"width": 64,
			"height": 32,
		},
	}, {
		"label": "FLOOR",
		"sprite": {
			"width": 64,
			"height": 64,
		},
		"tile": {
			"width": 64,
			"height": 32,
		},
	}, {
		"label": "WALL",
		"sprite": {
			"width": 64,
			"height": 64,
		},
		"tile": {
			"width": 64,
			"height": 32,
		},
	},
]

func _ready():
	for template in TEMPLATES:
		template_options.add_item(template.label)
	template_options.connect("item_selected", self, "_on_template_selected")
	# not sure why they appear twice , temp fix
	if template_options.get_item_count() > TEMPLATES.size():
		for i in range(2 * TEMPLATES.size() - 1, TEMPLATES.size() - 1, -1):
			template_options.remove_item(i)


func _on_template_selected(selected_id):
	if selected_id == 0:
		return
	var template = TEMPLATES[selected_id]
	sprite_width.set_value(template.sprite.width)
	sprite_height.set_value(template.sprite.height)
	tile_width.set_value(template.tile.width)
	tile_height.set_value(template.tile.height)
	
func get_tile_properties():
	if sprite_width.get_value() == 0 or sprite_height.get_value() == 0 or tile_width.get_value() == 0 or tile_height.get_value() == 0:
		return null
	return {
		"sprite": {
			"width": sprite_width.get_value(),
			"height": sprite_height.get_value(),
		},
		"tile": {
			"width": tile_width.get_value(),
			"height": tile_height.get_value(),
		},
	}
	