## Tileset importer for Godot Engine 3

With the tileset-importer plugin, you can convert PNG files to both the Godot scenes you'd typically make and then export, and the exported tileset resource file itself. This allows for finetuning in the scene where it'd be necessary.

If you're planning on having multiple or large tilesets, this should be the tool for you!

### Usage

The plugin will appear like this in your editor:

![alt text][preview]

#### Defaults

The first thing I recommend you to do is make your own templates and default export folders.

You can find these:
* TEMPLATES in src/tile_properties.gd
* Scene folder and Tileset folder in src/file_properties.gd

These will now default to your own, complimenting your workflow. If you're making an isometric game using 64x32 tiles, you can probably use the pre-existing ones.

This can be expanded to external configs if time permits.


#### Importing an image

To import a PNG or JPG (the current filters), do the following:

* Fill in the sizes of your sprites and tiles respectively, or select a template
    * The semantic difference between tile height and sprite height is that tile height is the actual height of the tile in your tilemap, while sprite height is the height of its sprite in the spritesheet. If the sprite height is larger than the tile height, which is the case for me, it adds an offset so that drawing them still centers them. The same applies to the width.
* Select an image to make as tileset.
* Choose a folder for both the tileset scene and the tileset resource
* (**Optional**) choose a folder name to have in these folders. Eg GrassLevels or so.
* Choose a file name for the tileset.
* Push transform!

There, that's it. You now have both of these with far less effort than would have otherwise been the case!

### Installation in your project

```shell
$ git clone https://gitlab.com/allindevstudios/godot-plugins/tileset-importer.git addons/tileset-importer
```
Or
Sort-of-advanced: if you want to add this as a git submodule
```shell
git submodule add https://gitlab.com/allindevstudios/godot-plugins/tileset-importer.git addons/tileset-importer
```

Then you can enable this plugin in Project Settings' Plugins tab

![alt text][project_settings]

[preview]: https://gitlab.com/allindevstudios/godot-plugins/tileset-importer/raw/master/documentation/images/preview.png "Open to new feature suggestions!"
[project_settings]: https://gitlab.com/allindevstudios/godot-plugins/tileset-importer/raw/master/documentation/images/project_settings.png "Please set it to Active if it appears! Press Update if it doesn't!"


### Planned features

**DISCLAIMER**: I only develop this plugin as I need it to improve. Not for the sake of developing this plugin. While I want to implement these features, my game goes first :smiley:.

* Error catching - things don't work when the user has faulty inputs, but this can be expanded beyond what is currently caught.
* Separating scene from tileset as an option for the user on what they want to export.
* Better presentation - I don't know how to make Control scenes pretty, and that reflects in this plugin.
* Tiled compatibility - reading Tiled json files would allow auto-naming of the tiles as well, and less manual configuration in the editor since it's already stored in a Tiled file.
* Naming reuse - now it overrides a pre-existing scene, but it could be possible to check the pre-existing scene and/or resource to re-use names and other meta properties of the tiles (such as collision areas)
* Open to suggestions...