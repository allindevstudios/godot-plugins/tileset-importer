tool
extends EditorPlugin

var dock

func _enter_tree():
	dock = preload("res://addons/tileset-importer/src/TileSetFromPNG.tscn").instance()

	add_control_to_dock(DOCK_SLOT_RIGHT_UL, dock)
	# Note that LEFT_UL means the left of the editor, upper-left dock

func _exit_tree():
	remove_control_from_docks(dock)
	dock.free()